package repositories

import (
	"context"
	"fmt"

	"gitlab.com/tigorlazuardi/shadowverse-go/v1/models"
)

func (repo *DBRepository) GetCards(ctx context.Context, query models.BaseQuery) models.ResponseData {
	query = query.AssignDefault()
	cards := []models.Card{}
	db := repo.DB.Connect()
	var total uint64
	db.
		Limit(query.Limit).
		Offset(query.Offset).
		Order(fmt.Sprintf("%s %s", query.Order, query.Sort)).
		Find(&cards).
		Offset(-1).
		Limit(-1).
		Count(&total)
	return models.ResponseData{
		Count: len(cards),
		Value: cards,
		Total: total,
	}
}

func (repo *DBRepository) SearchCards(ctx context.Context, query models.SearchQuery) models.ResponseData {
	query = *query.AssignDefault()
	cards := []models.Card{}
	db := repo.DB.Connect()
	var total uint64
	db.Where("LOWER(card_name) LIKE LOWER(?)", query.Search).
		Or("LOWER(description) LIKE LOWER(?)", query.Search).
		Or("LOWER(evo_description) LIKE LOWER(?)", query.Search).
		Or("LOWER(skill_disc) LIKE LOWER(?)", query.Search).
		Or("LOWER(evo_skill_disc) LIKE LOWER(?)", query.Search).
		Limit(query.Limit).
		Offset(query.Offset).
		Order(fmt.Sprintf("%s %s", query.Order, query.Sort)).
		Find(&cards).
		Offset(-1).
		Limit(-1).
		Count(&total)
	return models.ResponseData{
		Count: len(cards),
		Total: total,
		Value: cards,
	}
}
