package repositories

import (
	"fmt"
	"strconv"

	"gitlab.com/tigorlazuardi/shadowverse-go/v1/models"
)

func (repo *DBRepository) Close() error {
	return repo.DB.Close()
}

func (repo *DBRepository) EmptyDB() {
	db := repo.DB.Connect()
	db.DropTableIfExists(&models.Card{})
}

func (repo *DBRepository) InsertNewCards(data []models.Card) {
	db := repo.DB.Connect()
	db.AutoMigrate(&models.Card{})
	workers := 10
	chanCard := make(chan models.Card, workers)
	for i := 0; i < workers; i++ {
		go repo.worker(chanCard)
	}
	for _, card := range data {
		strID := strconv.FormatUint(uint64(card.CardID), 10)
		if card.CardName != "" {
			card.ImageLink = fmt.
				Sprintf(`https://shadowverse-portal.com/image/card/phase2/common/C/C_%s.png`,
					strID,
				)
			card.NameImageLink = fmt.
				Sprintf(`https://shadowverse-portal.com/image/card/phase2/common/N/N_%s.png`, strID)
			if card.EvoDescription != "" {
				card.EvoImageLink = fmt.
					Sprintf(
						`https://shadowverse-portal.com/image/card/phase2/common/E/E_%s.png`,
						strID,
					)
			}
			chanCard <- card
		}
	}
	close(chanCard)
}

func (repo *DBRepository) worker(cards <-chan models.Card) {
	db := repo.DB.Connect()
	for card := range cards {
		db.Create(&card)
	}
}
