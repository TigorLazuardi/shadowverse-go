package repositories

import (
	"context"

	"gitlab.com/tigorlazuardi/shadowverse-go/v1/infrastructures"
	"gitlab.com/tigorlazuardi/shadowverse-go/v1/models"
)

type DBRepository struct {
	DB infrastructures.IGorm
}

type IDBRepository interface {
	Close() error
	EmptyDB()
	InsertNewCards(data []models.Card)
	GetCards(ctx context.Context, query models.BaseQuery) models.ResponseData
	SearchCards(ctx context.Context, query models.SearchQuery) models.ResponseData
}

func NewDBRepository() IDBRepository {
	return &DBRepository{
		DB: infrastructures.Gorm{},
	}
}
