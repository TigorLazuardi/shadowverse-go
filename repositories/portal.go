package repositories

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/tigorlazuardi/shadowverse-go/v1/models"
)

type PortalRepository struct{}

type IPortalRepository interface {
	GetLatestCards() (data []models.Card, err error)
}

func NewPortalRepository() IPortalRepository {
	return PortalRepository{}
}

func (PortalRepository) GetLatestCards() (data []models.Card, err error) {
	req, err := http.Get("https://shadowverse-portal.com/api/v1/cards?format=json&lang=en")
	if err != nil {
		return
	}
	defer req.Body.Close()

	var w models.Portal
	err = json.NewDecoder(req.Body).Decode(&w)
	if err != nil {
		fmt.Println(err)
		return
	}
	return w.Data.Cards, nil
}
