package main

import (
	"gitlab.com/tigorlazuardi/shadowverse-go/v1/cmd"
)

// @title Shadowverse-Go API
// @version 0.1.0
// @description Shadowverse-Go is a binary that connects to Postgresql database and do services like searching for the cards

// @license.name MIT
// @license.url https://gitlab.com/TigorLazuardi/shadowverse-go/-/blob/master/LICENSE

// @contact.name TigorLazuardi
// @contact.email tigor.hutasuhut@gmail.com
// @host 206.189.149.81:8000
// @BasePath /v1
func main() {
	cmd.Exec()
}
