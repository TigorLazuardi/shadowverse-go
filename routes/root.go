package routes

import (
	"context"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	echoSwagger "github.com/swaggo/echo-swagger"
	"gitlab.com/tigorlazuardi/shadowverse-go/v1/controllers"
	_ "gitlab.com/tigorlazuardi/shadowverse-go/v1/docs"
	"gitlab.com/tigorlazuardi/shadowverse-go/v1/helpers"
)

type Router struct {
	Echo    *echo.Echo
	control controllers.Controller
}

func NewRouter(ctx context.Context) Router {
	e := echo.New()
	e.Use(middleware.Recover())
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: `{"time":"${time_rfc3339}","id":"${id}","remote_ip":"${remote_ip}","host":"${host}",` +
			`"method":"${method}","uri":"${uri}","status":${status},"error":"${error}","latency":${latency},` +
			`"latency_human":"${latency_human}","bytes_in":${bytes_in},` +
			`"bytes_out":${bytes_out}}` + "\n",
	}))
	e.Use(middleware.CORS())
	e.HTTPErrorHandler = errorHandler
	r := Router{Echo: e, control: controllers.NewController(ctx)}

	root := e.Group("")
	v1 := e.Group("/v1")
	v1.GET("/docs/*", echoSwagger.WrapHandler)
	cards := v1.Group("/cards")
	r.baseRoute(root)
	r.cardRoute(cards)
	return r
}

func (ro *Router) baseRoute(g *echo.Group) {
	g.GET("/", ro.control.HealthCheck)
	g.GET("/__routes", ro.control.PrintRoutes)
}

func errorHandler(err error, c echo.Context) {
	helpers.ResponseErrorWithLog(c, err)
}
