package routes

import (
	"github.com/labstack/echo/v4"
)

func (ro *Router) cardRoute(g *echo.Group) {
	g.GET("/", ro.control.GetCards)
	g.GET("/search", ro.control.SearchCards)
	g.GET("/__update_db", ro.control.UpdateDB)
}
