package cmd

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/tigorlazuardi/shadowverse-go/v1/routes"
)

var restCmd = &cobra.Command{
	Use:     "serve",
	Short:   "Serves REST API",
	Long:    `Serves REST API for Shadowverse cards.`,
	Example: "shadowverse-go serve -x cybertruck -u Elon -d 127.0.0.1:5432 -t tesla",
	Run: func(cobra *cobra.Command, args []string) {
		viper.BindPFlags(cobra.Flags())
		ctx, exit := context.WithCancel(context.Background())
		sig := make(chan os.Signal)
		signal.Notify(sig, syscall.SIGTERM, syscall.SIGINT)

		r := routes.NewRouter(ctx)
		server := http.Server{
			Addr:    fmt.Sprintf("0.0.0.0:%s", viper.GetString("port")),
			Handler: r.Echo,
		}
		go func() {
			fmt.Println("Server listening on port:", viper.GetString("port"))
			log.Fatal(server.ListenAndServe())
		}()
		<-sig
		exit()
		fmt.Println("Exit signal received. Please wait at most 30 seconds for the app to close.")
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
		defer cancel()
		if err := server.Shutdown(ctx); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	},
}

func init() {
	var port uint16
	var password string
	var username string
	var host string
	var name string
	var dbPort uint16

	restCmd.Flags().
		StringVarP(&password, "db-pass", "x", "", "Database Password [REQUIRED]")
	restCmd.MarkFlagRequired("db-pass")

	restCmd.Flags().
		StringVarP(&username, "db-user", "u", "", "Database Username [REQUIRED]")
	restCmd.MarkFlagRequired("db-user")

	restCmd.Flags().
		StringVarP(&name, "db-target", "t", "", "Database Target [REQUIRED]")
	restCmd.MarkFlagRequired("db-target")

	restCmd.Flags().
		StringVarP(&host, "db-host", "d", "", "Database Host [REQUIRED]")
	restCmd.MarkFlagRequired("db-host")

	restCmd.Flags().
		Uint16VarP(&dbPort, "db-port", "p", 5432, "Database Port")

	restCmd.Flags().
		Uint16Var(&port, "port", 8080, "Overrides the default port used")

	rootCmd.AddCommand(restCmd)
}
