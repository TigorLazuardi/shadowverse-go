package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "shadowverse-go",
	Short: "shadowverse-go is an API to handle cards of Shadowverse database",
	Long: `shadowverse-go is an API to handle cards of Shadowverse database.
It also gives cron function so systemd or to do 'Run once' jobs`,
}

func Exec() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
