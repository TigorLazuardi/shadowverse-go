package cmd

import (
	"context"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/tigorlazuardi/shadowverse-go/v1/services"
)

var cronCmd = &cobra.Command{
	Use:     "cron",
	Short:   "Updates database for new cards",
	Long:    `Updates database for new cards in 'run once' manner.`,
	Example: "shadowverse-go cron -x cybertruck -u Elon",
	Run: func(cobra *cobra.Command, args []string) {
		viper.BindPFlags(cobra.Flags())
		prefix := "Cron ==>"
		cron := services.NewCronService(context.Background())
		fmt.Println(prefix, "Updating database...")
		err := cron.UpdateDB()
		if err != nil {
			fmt.Println(prefix, err.Error())
			os.Exit(1)
		}
		fmt.Println(prefix, "Database updated")
	},
}

func init() {
	var password string
	var username string
	var host string
	var name string
	var dbPort uint16

	cronCmd.Flags().
		StringVarP(&password, "db-pass", "x", "", "Database Password [REQUIRED]")
	cronCmd.MarkFlagRequired("db-pass")

	cronCmd.Flags().
		StringVarP(&username, "db-user", "u", "", "Database Username [REQUIRED]")
	cronCmd.MarkFlagRequired("db-user")

	cronCmd.Flags().
		StringVarP(&name, "db-target", "t", "", "Database Target [REQUIRED]")
	cronCmd.MarkFlagRequired("db-target")

	cronCmd.Flags().
		StringVarP(&host, "db-host", "d", "", "Database Host [REQUIRED]")
	cronCmd.MarkFlagRequired("db-host")

	cronCmd.Flags().
		Uint16VarP(&dbPort, "db-port", "p", 5432, "Database Port")

	rootCmd.AddCommand(cronCmd)
}
