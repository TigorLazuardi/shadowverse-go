
FROM golang:1.14.6-alpine AS builder
WORKDIR /app
COPY go.mod .
RUN go mod download
COPY . .
RUN go build -o shadowverse-go

FROM alpine:3.12 AS runtime
ENV DATABASE_USERNAME=""
ENV DATABASE_PASSWORD=""
ENV DATABASE_NAME=""
ENV DATABASE_HOST=""
ENV DATABASE_PORT=5432
ENV PORT=8080
COPY --from=builder /app/shadowverse-go .
CMD ./shadowverse-go serve \
    -x ${DATABASE_PASSWORD} \
    -u ${DATABASE_USERNAME} \
    -p ${DATABASE_PORT} \
    -t ${DATABASE_NAME} \
    -d ${DATABASE_HOST} \
    --port ${PORT}
