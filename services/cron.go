package services

import (
	"context"

	"gitlab.com/tigorlazuardi/shadowverse-go/v1/models"
	"gitlab.com/tigorlazuardi/shadowverse-go/v1/repositories"
)

type CronService struct {
	Portal repositories.IPortalRepository
	DB     repositories.IDBRepository
	ctx    context.Context
}

type ICronService interface {
	UpdateDB() (err error)
}

func NewCronService(ctx context.Context) ICronService {
	return &CronService{
		Portal: repositories.NewPortalRepository(),
		DB:     repositories.NewDBRepository(),
		ctx:    ctx,
	}
}

func (s *CronService) UpdateDB() (err error) {
	data, err := s.Portal.GetLatestCards()
	if err != nil {
		return models.Response{
			Code:    424,
			Data:    models.ResponseData{},
			Err:     err.Error(),
			Message: "Failed to get latest cards from portal",
		}
	}
	s.DB.EmptyDB()
	s.DB.InsertNewCards(data)
	return
}
