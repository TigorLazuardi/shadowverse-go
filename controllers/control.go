package controllers

import (
	"context"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/tigorlazuardi/shadowverse-go/v1/repositories"
	"gitlab.com/tigorlazuardi/shadowverse-go/v1/services"
)

type Controller struct {
	cron services.ICronService
	ctx  context.Context
	card repositories.IDBRepository
}

func NewController(ctx context.Context) Controller {
	return Controller{
		cron: services.NewCronService(ctx),
		card: repositories.NewDBRepository(),
	}
}

func (w *Controller) HealthCheck(c echo.Context) error {
	return c.JSON(http.StatusOK, map[string]string{
		"message": "server is alive",
	})
}

func (w *Controller) PrintRoutes(c echo.Context) error {
	e := c.Echo()
	return c.JSON(http.StatusOK, e.Routes())
}
