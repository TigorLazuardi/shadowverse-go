package controllers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/tigorlazuardi/shadowverse-go/v1/helpers"
	"gitlab.com/tigorlazuardi/shadowverse-go/v1/models"
)

// GetCards godoc
// @Summary Get cards from database. Maximum 100.
// @Description Fetch cards from database.
// @Tags cards
// @Accept json
// @Produce json
// @Param query query models.BaseQuery false "comment"
// @Success 200 {object} models.Response{data=models.ResponseData{value=[]models.Card}}
// @Failure 500 {object} models.Response{data=models.ResponseData{value=string}}
// @Router /cards/ [GET]
func (w *Controller) GetCards(c echo.Context) (err error) {
	q := models.BaseQuery{}
	if err = c.Bind(&q); err != nil {
		return
	}
	data := w.card.GetCards(w.ctx, q)
	return c.JSON(http.StatusOK, models.Response{
		Code:    200,
		Data:    data,
		Message: "OK",
	})
}

func (w *Controller) SearchCards(c echo.Context) (err error) {
	q := models.SearchQuery{}
	if err = c.Bind(&q); err != nil {
		return
	}
	if q.Search == "" {
		return models.Response{
			Code:    400,
			Err:     "Bad Request",
			Message: "Search string required.",
		}
	}
	data := w.card.SearchCards(w.ctx, q)
	return c.JSON(http.StatusOK, models.Response{
		Code:    200,
		Data:    data,
		Message: "OK",
	})
}

func (w *Controller) UpdateDB(c echo.Context) (err error) {
	err = w.cron.UpdateDB()
	if err != nil {
		return
	}
	return helpers.ResponseWithLog(c, models.Response{
		Code: 200,
		Data: models.ResponseData{
			Count: 0,
			Total: 0,
			Value: "OK",
		},
		Message: "Database updated",
		Err:     "",
	})
}
