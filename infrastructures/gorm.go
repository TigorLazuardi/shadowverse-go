package infrastructures

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/spf13/viper"
	"gitlab.com/tigorlazuardi/shadowverse-go/v1/models"
)

var db *gorm.DB

type IGorm interface {
	Connect() *gorm.DB
	Close() error
}

type Gorm struct{}

func (g Gorm) Connect() *gorm.DB {
	if db == nil {
		host := viper.GetString("db-host")
		port := viper.GetInt("db-port")
		username := viper.GetString("db-user")
		password := viper.GetString("db-pass")
		dbname := viper.GetString("db-target")
		uri := fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s sslmode=disable", host, port, username, dbname, password)
		database, err := gorm.Open("postgres", uri)
		if err != nil {
			panic(err)
		}
		db = database

		db.AutoMigrate(&models.Card{})
	}
	return db
}

func (g Gorm) Close() error {
	if db == nil {
		return nil
	}
	return db.Close()
}
