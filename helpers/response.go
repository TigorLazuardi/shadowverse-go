package helpers

import (
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/tigorlazuardi/shadowverse-go/v1/models"
)

var (
	stdout *logrus.Logger
	stderr *logrus.Logger
)

func init() {
	stdout = &logrus.Logger{
		Out: os.Stdout,
		Formatter: &logrus.JSONFormatter{
			FieldMap: logrus.FieldMap{
				logrus.FieldKeyMsg: "message",
			},
		},
		Hooks: make(logrus.LevelHooks),
		Level: logrus.InfoLevel,
	}
	stderr = &logrus.Logger{
		Formatter: &logrus.JSONFormatter{
			FieldMap: logrus.FieldMap{
				logrus.FieldKeyMsg: "message",
			},
		},
		Hooks: make(logrus.LevelHooks),
		Level: logrus.ErrorLevel,
	}
}

func ResponseWithLog(c echo.Context, response models.Response) error {
	stdout.WithFields(map[string]interface{}{
		"code":  response.Code,
		"data":  response.Data,
		"error": response.Err,
	}).Print(response.Message)
	return c.JSON(http.StatusOK, response)
}

func ResponseErrorWithLog(c echo.Context, response error) {
	if e, ok := response.(models.Response); ok {
		stderr.WithFields(map[string]interface{}{
			"code":  e.Code,
			"data":  e.Data,
			"error": e.Err,
		}).Error(e.Message)
		c.JSON(http.StatusOK, e)
		return
	}
	res := make(map[string]interface{})
	res["code"] = 500
	res["data"] = response.Error()
	res["message"] = response.Error()
	res["error"] = response.Error()
	stderr.WithFields(res).Error(res["message"])
	c.JSON(http.StatusOK, res)
}
