package models

import (
	"time"
)

type Card struct {
	ID              uint       `gorm:"primary_key" json:"id" db:"id" example:"900"`
	CreatedAt       time.Time  `json:"created_at" db:"created_at"`
	UpdatedAt       time.Time  `json:"updated_at" db:"updated_at"`
	DeletedAt       *time.Time `sql:"index" json:"deleted_at" db:"deleted_at"`
	CardID          uint       `json:"card_id" db:"card_id" gorm:"primary_key"`
	FoilCardID      uint       `json:"foil_card_id" db:"foil_card_id"`
	CardSetID       uint       `json:"card_set_id" db:"card_set_id"`
	CardName        string     `json:"card_name" db:"card_name"`
	IsFoil          uint8      `json:"is_foil" db:"is_foil"`
	CharType        uint8      `json:"char_type" db:"char_type"`
	Clan            uint8      `json:"clan" db:"clan"`
	TribeName       string     `json:"tribe_name" db:"tribe_name"`
	Skill           string     `json:"skill" db:"skill"`
	SkillCondition  string     `json:"skill_condition" db:"skill_condition"`
	SkillTarget     string     `json:"skill_target" db:"skill_target"`
	SkillOption     string     `json:"skill_option" db:"skill_option"`
	SkillPreprocess string     `json:"skill_preprocess" db:"skill_preprocess"`
	SkillDisc       string     `json:"skill_disc" db:"skill_disc"`
	OrgSkillDisc    string     `json:"org_skill_disc" db:"org_skill_disc"`
	EvoSkillDisc    string     `json:"evo_skill_disc" db:"evo_skill_disc"`
	Cost            uint8      `json:"cost" db:"cost"`
	Atk             uint8      `json:"atk" db:"atk"`
	Life            uint8      `json:"life" db:"life"`
	EvoAtk          uint8      `json:"evo_atk" db:"evo_atk"`
	EvoLife         uint8      `json:"evo_life" db:"evo_life"`
	Rarity          uint8      `json:"rarity" db:"rarity"`
	GetRedEther     uint       `json:"get_red_ether" db:"get_red_ether"`
	UseRedEther     uint       `json:"use_red_ether" db:"use_red_ether"`
	Description     string     `json:"description" db:"description"`
	EvoDescription  string     `json:"evo_description" db:"evo_description"`
	CV              string     `json:"cv" db:"cv"`
	Copyright       string     `json:"copyright" db:"copyright"`
	BaseCardID      uint32     `json:"base_card_id" db:"base_card_id"`
	NormalCardID    uint32     `json:"normal_card_id" db:"normal_card_id"`
	FormatType      uint8      `json:"format_type" db:"format_type"`
	RestrictedCount uint8      `json:"restricted_count" db:"restricted_count"`
	ImageLink       string     `json:"image_link" db:"image_link"`
	EvoImageLink    string     `json:"evo_image_link" db:"evo_image_link"`
	NameImageLink   string     `json:"name_image_link" db:"name_image_link"`
}
