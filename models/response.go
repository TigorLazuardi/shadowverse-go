package models

type Response struct {
	Code    uint16       `json:"code"`
	Data    ResponseData `json:"data"`
	Err     string       `json:"error"`
	Message string       `json:"message"`
}

func (r Response) Error() string { return r.Err }

type ResponseData struct {
	Count int         `json:"count"`
	Value interface{} `json:"value"`
	Total uint64      `json:"total"`
}
