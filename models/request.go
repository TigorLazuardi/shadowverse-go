package models

import "strings"

type BaseQuery struct {
	// The amount of json data to receive. Defaults to 25, maximum 100.
	Limit uint64 `json:"limit" form:"limit" query:"limit" maximum:"100" default:"25"`
	// The data that is skipped
	Offset uint64 `json:"offset" form:"offset" query:"offset" default:"0"`
	// Any supported card field. E.G. "card_name", "description"
	Order string `json:"order" form:"order" query:"order" default:"card_name"`
	// Sort order
	Sort string `json:"sort" form:"sort" query:"sort" default:"asc" enums:"asc,desc"`
}

func (b BaseQuery) AssignDefault() BaseQuery {
	if b.Limit == 0 {
		b.Limit = 25
	}
	if b.Limit > 100 {
		b.Limit = 100
	}
	if b.Order == "" {
		b.Order = "card_name"
	}
	if strings.ToLower(b.Sort) != "desc" {
		b.Sort = "asc"
	}
	return b
}

type SearchQuery struct {
	Search string `json:"q" from:"q" query:"q"`
	BaseQuery
}

func (s *SearchQuery) AssignDefault() *SearchQuery {
	s.Search = "%" + s.Search + "%"
	s.BaseQuery = s.BaseQuery.AssignDefault()
	return s
}

type CardQuery struct {
	Card
	BaseQuery
}

func (c *CardQuery) AssignDefault() *CardQuery {
	c.BaseQuery = c.BaseQuery.AssignDefault()
	return c
}
