package models

type Portal struct {
	DataHeaders PortalDataHeaders `json:"data_headers"`
	Data        PortalData        `json:"data"`
}

type PortalDataHeaders struct {
	UDID       bool   `json:"udid"`
	ViewerID   uint   `json:"viewer_id"`
	SID        string `json:"sid"`
	ServerTime uint64 `json:"server_time"`
	ResultCode uint8  `json:"result_code"`
}

type PortalData struct {
	Cards  []Card        `json:"cards"`
	Errors []interface{} `json:"errors"`
}
